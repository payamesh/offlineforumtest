import React from 'react';
import { mount, render } from 'enzyme';
import App from '../components/App';

test('renders the app', () => {
  render(<App />);
});

test('Change page', () => {
const wrapper = mount(<App />)
wrapper.find('select').simulate('change', ({target:{value:'Esmeralda'}}))
wrapper.find('button').simulate('click');
})

