import React from 'react';
import {mount} from 'enzyme';
import AvatarSelector from '../components/AvatarSelector'
import zac from '../images/zac.png';
import esmeralda from '../images/esmeralda.png';


test('find Zac', () =>{
    const wrapper = mount(<AvatarSelector currentPersona="Esmeralda" />);
    expect(wrapper.find('img').prop('src')).toEqual(esmeralda);
})

test('find no person ( zac )', () =>{       //default is Zac
    const wrapper = mount(<AvatarSelector currentPersona="payam" />);
    expect(wrapper.find('img').prop('src')).toEqual(zac) 
})