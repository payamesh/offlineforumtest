import React from 'react';
import {
    mount
} from 'enzyme';
import * as api from '../api';

import SinglePost from '../components/SinglePost';
import Posts from '../components/Posts'

test(' Create and delete posts', () => {
    const wrapper = mount( <Posts currentPersona="Zac" author="Zac" postID="123"/> );
    //create
    wrapper.find('#title').simulate('change', {target:{name:'title', value:'Min title' } })
    wrapper.find('#content').simulate('change', {target:{name:'content', value:'Min content' } })
    wrapper.find('input[type="submit"]').simulate('submit')
    //delete
    const postid = api.fetchAllPosts();
    api.removePost(postid[0].id)
    console.log(api.fetchAllPosts()) //nada
});



