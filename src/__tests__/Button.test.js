import React from 'react';
import { mount } from 'enzyme';
import Button from '../components/Button';
import dangerStyle from '../components/Button';

test('check dangerStyle', () => {
    const fakeClick = jest.fn();
    const wrapper = mount(<Button onClick={fakeClick} children="x" danger={true} />);
    expect(wrapper.find('button').hasClass('bg-red-dark')).toBe(true)
  });