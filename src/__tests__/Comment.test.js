import React from 'react';
import {
    mount
} from 'enzyme';
import SingleComment from '../components/SingleComment';
import SinglePost from '../components/SinglePost';


test('find Comment', () => {
    const fakeClick = jest.fn();
    const wrapper = mount( <SingleComment id = "1"
        author = "payye"
        onClick = {
            fakeClick
        }
        currentPersona = "payye"
        comment = "Hej detta är en kommentar"
        date = "2020-01-01" /> );
    const commentAuthor = wrapper.find('.text-grey-dark');
    const commentContent = wrapper.find('.text-grey-darker');
        expect(commentAuthor.text()).toMatch('payye');
        expect(commentContent.text()).toMatch('Hej detta är en kommentar');
});

test('check if comments are created', () =>{
    const fakeClick = jest.fn();
    const wrapper = mount(<SinglePost id = "1"
    title="Hello"
    content="this is content"
    author = "payye"
    onClick = {
        fakeClick
    }
    currentPersona = "payye"
    comment = "Hej detta är en kommentar"
    date = "2020-01-01"
         />)
    wrapper.find('#comment').simulate('change',  { target: { name: 'comment', value: 'sometext' } })
    expect(wrapper.find('textarea').text()).toMatch('sometext') //check if comment exists
    wrapper.find('input[type="submit"]').simulate('submit')
    expect(wrapper.find('[data-test="button"]')).toHaveLength(2) //check if comment added
        expect(wrapper.find('.text-grey-darker').text()).toMatch('sometext');
        wrapper.find('#commentContainer').children().at(0).simulate('click')
        expect(wrapper.find('.text-grey-darker')).toHaveLength(0)
})